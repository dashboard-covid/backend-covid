"""Urls for project.

Link to admin, show dashboard and main page.
"""
from django.urls import path, include
from .views import main_page, covid
from django.conf import settings

urlpatterns = [
    path('', main_page),
    path('covid', covid),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
                      path('__debug__/', include(debug_toolbar.urls)),
                  ] + urlpatterns
