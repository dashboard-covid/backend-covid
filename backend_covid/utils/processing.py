import datetime
import numpy as np
import pandas as pd

from django.db.models import Count
from covidmx import CovidMX
from backend_covid.models import CovidRegistry


def get_date_to_extract_data():
    today = (datetime.date.today() - datetime.timedelta(days=2))
    if today.weekday() > 4:
        today = today - datetime.timedelta(days=today.weekday() - 4)
    return today.strftime('%d-%m-%Y'), today.strftime('%Y-%m-%d')


def process_data(date):
    data = CovidMX(source='Serendipia', date=date, kind="confirmed").get_data()
    data['infectado'] = data.apply(
        lambda a: "0-10"
        if a['edad'] <= 11 else "12-18"
        if 12 <= a['edad'] <= 18 else "19-35"
        if 19 <= a['edad'] <= 35 else "36-60"
        if 36 <= a['edad'] <= 60 else "60-130",
        axis=1)
    data['fecha_sintomas'] = pd.to_datetime(data['fecha_sintomas'])
    data['fecha_busqueda'] = pd.to_datetime(data['fecha_busqueda'])
    data['mes_sintomas'] = data['fecha_sintomas'].dt.month
    data['año_sintomas'] = data['fecha_sintomas'].dt.year
    data['mes_sintomas'] = data['mes_sintomas'].astype(str)
    data['año_sintomas'] = data['año_sintomas'].astype(str)
    data['dias_transcurridos'] = (data['fecha_busqueda'] - data['fecha_sintomas']).dt.days
    # Process Data
    res = data.groupby(['nom_ent','infectado','sexo','mes_sintomas','año_sintomas']).agg(['count'])['id_registro']
    res.rename(columns={'infectado':'n'}, inplace=True)
    res.reset_index(inplace=True)
    res.rename(columns={"nom_ent":"estado", "count":"cantidad"}, inplace=True)

    res = pd.pivot_table(res, index=["estado"], values=["cantidad"],
                         columns=['mes_sintomas', 'año_sintomas', "infectado", "sexo"],
                         aggfunc={"cantidad": np.sum}, fill_value=0).swaplevel(0, 1, axis=1)
    res.columns = res.columns.map('_'.join)
    res.reset_index(inplace=True)

    res = res.set_index('estado')
    res = res.T
    res.reset_index(level=0, inplace=True)

    return res


def validate_if_date_exists_in_registry(date):
    if CovidRegistry.objects.filter(search_date=date).count() > 0:
        return True
    return False


def get_registry(date):
    return list(CovidRegistry.objects.filter(search_date=date).values())


def save_data(data, date):
    for index, row in data.iterrows():
        registry = CovidRegistry(
            search_date=date,
            type_info=row[0],
            Aguascalientes=row['Aguascalientes'],
            Baja_California=row['Baja California'],
            Baja_California_Sur=row['Baja California Sur'],
            Campeche=row['Campeche'],
            Chiapas=row['Chiapas'],
            Chihuahua=row['Chihuahua'],
            Ciudad_de_Mexico=row['Ciudad de México'],
            Coahuila_de_Zaragoza=row['Coahuila de Zaragoza'],
            Colima=row['Colima'],
            Durango=row['Durango'],
            Guanajuato=row['Guanajuato'],
            Guerrero=row['Guerrero'],
            Hidalgo=row['Hidalgo'],
            Jalisco=row['Jalisco'],
            Michoacan_de_Ocampo=row['Michoacán de Ocampo'],
            Morelos=row['Morelos'],
            Mexico=row['México'],
            Nayarit=row['Nayarit'],
            Nuevo_Leon=row['Nuevo León'],
            Oaxaca=row['Oaxaca'],
            Puebla=row['Puebla'],
            Queretaro=row['Querétaro'],
            Quintana_Roo=row['Quintana Roo'],
            San_Luis_Potosi=row['San Luis Potosí'],
            Sinaloa=row['Sinaloa'],
            Sonora=row['Sonora'],
            Tabasco=row['Tabasco'],
            Tamaulipas=row['Tamaulipas'],
            Tlaxcala=row['Tlaxcala'],
            Veracruz_de_Ignacio_de_la_Llave=row['Veracruz de Ignacio de la Llave'],
            Yucatan=row['Yucatán'],
            Zacatecas=row['Zacatecas']
        )
        registry.save()
