"""API Views.

Show views of the main application.
"""
from django.http import JsonResponse
from backend_covid.utils.processing import get_date_to_extract_data, process_data, save_data, \
    validate_if_date_exists_in_registry, get_registry


def main_page(request):
    """Main view API

    Main view app
    """
    return JsonResponse({"message": "COVID API 1.0"})


def covid(request):
    """Process Data
    """
    # Get last data info
    date_f1, date_f2 = get_date_to_extract_data()

    if not validate_if_date_exists_in_registry(date_f2):
        data = process_data(date_f1)
        save_data(data, date_f2)

    result = get_registry(date_f2)
    
    return JsonResponse(result, safe=False)
