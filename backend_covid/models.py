from django.db import models


class CovidRegistry(models.Model):
    search_date = models.DateTimeField(
        null=True,
        blank=True,
    )

    type_info = models.CharField(max_length=80, blank=False, null=False)

    Aguascalientes = models.IntegerField(blank=False, null=False)
    Baja_California = models.IntegerField(blank=False, null=False)
    Baja_California_Sur = models.IntegerField(blank=False, null=False)
    Campeche = models.IntegerField(blank=False, null=False)
    Chiapas = models.IntegerField(blank=False, null=False)
    Chihuahua = models.IntegerField(blank=False, null=False)
    Ciudad_de_Mexico = models.IntegerField(blank=False, null=False)
    Coahuila_de_Zaragoza = models.IntegerField(blank=False, null=False)
    Colima = models.IntegerField(blank=False, null=False)
    Durango = models.IntegerField(blank=False, null=False)
    Guanajuato = models.IntegerField(blank=False, null=False)
    Guerrero = models.IntegerField(blank=False, null=False)
    Hidalgo = models.IntegerField(blank=False, null=False)
    Jalisco = models.IntegerField(blank=False, null=False)
    Michoacan_de_Ocampo = models.IntegerField(blank=False, null=False)
    Morelos = models.IntegerField(blank=False, null=False)
    Mexico = models.IntegerField(blank=False, null=False)
    Nayarit = models.IntegerField(blank=False, null=False)
    Nuevo_Leon = models.IntegerField(blank=False, null=False)
    Oaxaca = models.IntegerField(blank=False, null=False)
    Puebla = models.IntegerField(blank=False, null=False)
    Queretaro = models.IntegerField(blank=False, null=False)
    Quintana_Roo = models.IntegerField(blank=False, null=False)
    San_Luis_Potosi = models.IntegerField(blank=False, null=False)
    Sinaloa = models.IntegerField(blank=False, null=False)
    Sonora = models.IntegerField(blank=False, null=False)
    Tabasco = models.IntegerField(blank=False, null=False)
    Tamaulipas = models.IntegerField(blank=False, null=False)
    Tlaxcala = models.IntegerField(blank=False, null=False)
    Veracruz_de_Ignacio_de_la_Llave = models.IntegerField(blank=False, null=False)
    Yucatan = models.IntegerField(blank=False, null=False)
    Zacatecas = models.IntegerField(blank=False, null=False)
