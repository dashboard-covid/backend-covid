"""Main views."""
from django.shortcuts import render
from django.http import HttpResponse


def handler404(request, *args, **argv):
    """Error 404 configuration."""
    return render(request, HttpResponse("Error 404"))


def handler500(request, *args, **argv):
    """Error 500 configuration."""
    return render(request, HttpResponse("Error 500"))
