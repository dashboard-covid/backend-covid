<div align="center">
  <img width="64" src="https://gitlab.com/uploads/-/system/project/avatar/21393021/iconfinder_geocoding_api_86221.png?width=64" alt="Backend Covid">
  <h3 align="center">Backend COVID</h3>
  <p align="center">
    <a href="https://github.com/ActivandoIdeas/Cookiecutter-Django-AppEngine-GitLab/blob/master/LICENSE">
      	<img src="https://img.shields.io/badge/License-BSD3-blue.svg"  alt="license badge"/>
    </a>
    <a href="https://travis-ci.org/ActivandoIdeas/Cookiecutter-Django-AppEngine-GitLab">
        <img src="https://img.shields.io/travis/ActivandoIdeas/Cookiecutter-Django-AppEngine-GitLab.svg?label=django-cookiecutter" alt="Build Status">
    </a>
    <a href="https://www.python.org/">
        <img src="https://img.shields.io/pypi/pyversions/Django.svg?style=flat-square"  alt="python badge">
    </a>
  </p>
</div>

## Demo

La api muestra devuelve la siguiente información

<div align="center">
  <img src="/img/api.png" alt="Dashboard Covid">
</div>

## Analisis y estructuración de los datos

Los datos provienen de Serendipia con un servicio procesado por la librería de Federico Garza

https://github.com/FedericoGarza/covidmx

Para conocer el dataset, y procesar los datos que se muestran en la API, se uso Google Colab

https://colab.research.google.com/drive/1B0W9k1GeyRXa58Uwlt_gvYJqkebzE4TK?usp=sharing

Durante el analisis se detectaron inconsistencias en los datos para la parte de las fechas, un problema tipico al extraer datos de un csv o excel, por lo que en el dashboard los casos se ven con resultados a futuro

> Conviene revisar y enviar un pull request al repositorio identificando el problema

Una captura de los datos

<div align="center">
  <img src="/img/error.png" alt="Dashboard Covid">
</div>

Al compararlo con el original desde aquí:

https://serendipia.digital/2020/03/datos-abiertos-sobre-casos-de-coronavirus-covid-19-en-mexico/

Se puede validar la información

## Características:

* Procesamiento de datos
* Persistencia de información, únicamente se hace una petición y un procesamiento por día, una vez hecho los resultados se guardan en SQLite

## Estructura del proyecto

* **core** (Configuraciones del proyecto)
* **enviroments** (Soporte para multiples ambientes con sus configuraciones aisladas)
* **img** (Imagenes para documentar el proyecto)
* **backend_covid** (Código de la api)

# Ejecución con Docker

## Aplicación de desarrollo
```sh
docker-compose up
```

## Aplicación para producción
```sh
docker-compose -f docker-compose.prod.yml up
```

## Algunos comandos para kubernetes

```sh
gcloud auth login
gcloud config set project [yourproject]

# Container registry:
gcloud auth configure-docker
docker tag backend_covid_web gcr.io/activandoideas/backend_covid_web
docker build -t gcr.io/activandoideas/backend_covid_web .
docker push gcr.io/activandoideas/backend_covid_web:latest

kubectl get pods
kubectl apply -f web-deployment.yaml
kubectl apply -f web-service.yaml
kubectl get all
kubectl get svc
kubectl port-forward svc/web 8000:8000
kubectl get nodes

docker login registry.gitlab.com
docker build -t registry.gitlab.com/dashboard-covid/backend-covid .
docker push registry.gitlab.com/dashboard-covid/backend-covid

kubectl create secret docker-registry regcred --docker-server=registry.gitlab.com --docker-username=k8s --docker-password=<token>

```

Commands

```sh
gcloud auth login
gcloud config set project [yourproject]
gcloud auth configure-docker
docker build -t gcr.io/activandoideas/backend_covid_api --build-arg SECRET=$CI_SECRET_PROD
docker push gcr.io/activandoideas/backend_covid_api
kubectl apply -f web-pod.yaml
kubectl logs covid
kubectl delete pod covid
kubectl get svc
kubectl get all
kubectl get nodes
```

## Contribución

* Revisar el código de conducta

# Licencia

BSD 3-Clause "New" or "Revised" License
View in https://github.com/ActivandoIdeas/Cookiecutter-Django-AppEngine-GitLab/blob/master/LICENSE