"""Development file configuration.

Simple configuration file.
"""
from core.settings.base_settings import *
import os

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "data" if os.environ.get("SECRET") is None else os.environ.get("SECRET")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
PRODUCTION = False
CLOUD = False

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/

ALLOWED_HOSTS = ["*"]

if DEBUG:
    MIDDLEWARE += (
        'debug_toolbar.middleware.DebugToolbarMiddleware',
        'debug_toolbar_force.middleware.ForceDebugToolbarMiddleware',
    )
    INSTALLED_APPS += (
        'debug_toolbar',
    )
    INTERNAL_IPS = ('127.0.0.1',)
    DEBUG_TOOLBAR_CONFIG = {
        'INTERCEPT_REDIRECTS': False,
    }

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": os.path.join(BASE_DIR, "db.sqlite3"),
    }
}
