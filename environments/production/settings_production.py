"""Production file configuration.

Simple configuration file.
"""
from core.settings.base_settings import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "data" if os.environ.get("SECRET") is None else os.environ.get("SECRET")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False
PRODUCTION = True

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/

ALLOWED_HOSTS = ["*"]

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": os.path.join(BASE_DIR, "db.sqlite3"),
    }
}
