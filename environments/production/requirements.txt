Django==3.1.1
PyYAML==5.3.1
covidmx==0.3.1
gunicorn==20.0.4
django-cors-headers==3.5.0