# Official base image
FROM python:3.8.6-slim-buster

# Work directory
WORKDIR api

# Install dependencies
COPY environments/development/requirements.txt requirements.txt
RUN  ["python3", "-m", "pip", "install", "-r", "requirements.txt"]

# Copy files
COPY manage.py ./
COPY core ./core
COPY backend_covid ./backend_covid
COPY entrypoint.sh ./
COPY environments/development/settings_development.py ./core/settings/settings.py

EXPOSE 8000

RUN chmod a+x entrypoint.sh

# Run commands
ENTRYPOINT ["/bin/bash", "-c", "./entrypoint.sh"]